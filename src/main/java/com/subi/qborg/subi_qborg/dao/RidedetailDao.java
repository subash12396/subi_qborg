package com.subi.qborg.subi_qborg.dao;

import com.subi.qborg.subi_qborg.dto.CustomerDTO;
import com.subi.qborg.subi_qborg.dto.RidedetailDTO;
import com.subi.qborg.subi_qborg.dto.VehicleDTO;
import com.subi.qborg.subi_qborg.entities.Customer;
import com.subi.qborg.subi_qborg.entities.Ridedetail;
import com.subi.qborg.subi_qborg.entities.Vehicle;
import com.subi.qborg.subi_qborg.mapper.CustomerMapper;
import com.subi.qborg.subi_qborg.mapper.RidedetailMapper;
import com.subi.qborg.subi_qborg.mapper.VehicleMapper;
import com.subi.qborg.subi_qborg.repositories.RidedetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RidedetailDao {

    @Autowired
    RidedetailRepository ridedetailRepository;

    @Autowired
    RidedetailMapper ridedetailMapper;

    @Autowired
    VehicleMapper vehicleMapper;

    @Autowired
    CustomerMapper customerMapper;

    public boolean saveNewRideDetail(RidedetailDTO ridedetailDTO) throws  Exception {
        Ridedetail ridedetail = ridedetailMapper.mapRideDetailDTOToRideDetailEntity(ridedetailDTO);
        try{
            ridedetailRepository.save(ridedetail);
        } catch (Exception ex){
            return false;
        }
        return true;
    }

   public RidedetailDTO getRideDetailByVehicle(VehicleDTO vehicleDTO ) throws  Exception {
        RidedetailDTO ridedetailDTO = new RidedetailDTO();
        Vehicle vehicle = vehicleMapper.mapVehicleDTOToVehicleEntity(vehicleDTO);
        Ridedetail ridedetail =  ridedetailRepository.getRideDetailByVehicle(vehicle.getId());
        ridedetailDTO = ridedetailMapper.mapRideDetailEntityToRideDetailDTO(ridedetail);
        return ridedetailDTO;
   }

    public RidedetailDTO getRideDetailByCustomer(CustomerDTO customerDTO ) throws  Exception {
        RidedetailDTO ridedetailDTO = new RidedetailDTO();
        Customer customer = customerMapper.mapCutomerCustomerDTOToEntity(customerDTO);
        Ridedetail ridedetail =  ridedetailRepository.getRideDetailByCustomer(customerDTO.getId());
        ridedetailDTO = ridedetailMapper.mapRideDetailEntityToRideDetailDTO(ridedetail);
        return ridedetailDTO;
    }


}
