package com.subi.qborg.subi_qborg.dao;

import com.subi.qborg.subi_qborg.dto.VehicleDTO;
import com.subi.qborg.subi_qborg.entities.Vehicle;
import com.subi.qborg.subi_qborg.mapper.VehicleMapper;
import com.subi.qborg.subi_qborg.repositories.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehicleDao {

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    VehicleMapper vehicleMapper;

    public VehicleDTO getVehicleDTOByPhoneNo(String PhoneNo) throws Exception {
        Vehicle vehicle = vehicleRepository.getVehicleByPhoneNo(PhoneNo);
        if(vehicle == null) {
            return null;
        }
        return vehicleMapper.mapVehicleEntityToVehicleDTO(vehicle);
    }

    public VehicleDTO getVehicleDTOByVehicleNumber(String vehicleNumber) throws Exception {
        Vehicle vehicle = vehicleRepository.getVehicleDTOByVehicleNumber(vehicleNumber);
        if(vehicle == null) {
            return null;
        }
        return vehicleMapper.mapVehicleEntityToVehicleDTO(vehicle);
    }


    public boolean saveNewVehicle(VehicleDTO vehicleDTO) throws  Exception{
        Vehicle vehicle = vehicleMapper.mapVehicleDTOToVehicleEntity(vehicleDTO);
        try {
            vehicleRepository.save(vehicle);
        } catch (Exception ex) {
            return  false;
        }
        return true;
    }
    public List<VehicleDTO> getAllAvailableVehicles() throws Exception{
        List<Vehicle> vehicleList = vehicleRepository.gettAllAvailableVehicle();
        return vehicleMapper.mapVehicleEntityListToVehicleDTOList(vehicleList);
    }

    public void updateVehicleStatus(VehicleDTO vehicleDTO,Integer status) throws  Exception{
        Vehicle vehicle = vehicleMapper.mapVehicleDTOToVehicleEntity(vehicleDTO);
        vehicleRepository.updateVehicleStatus(vehicle.getId(),status);
    }




}
