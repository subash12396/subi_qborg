package com.subi.qborg.subi_qborg.dao;

import com.subi.qborg.subi_qborg.dto.CustomerDTO;
import com.subi.qborg.subi_qborg.entities.Customer;
import com.subi.qborg.subi_qborg.mapper.CustomerMapper;
import com.subi.qborg.subi_qborg.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerDao {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerMapper customerMapper;

    public CustomerDTO getCustomerDTOByEmailId(String emailId) throws Exception {
        Customer customer = customerRepository.getCutomerByEmailId(emailId);
        if (customer == null) {
            return null;
        }
        return customerMapper.mapCutomerEntityToCustomerDTO(customer);
    }

    public boolean saveNewCustomer(CustomerDTO customerDTO) throws  Exception{
        Customer customer = customerMapper.mapCutomerCustomerDTOToEntity(customerDTO);
        try {
            customerRepository.save(customer);
        } catch (Exception ex) {
            return  false;
        }
        return true;
    }

    public CustomerDTO getCustomerDTOByPhoneNo(String phoneNo) throws Exception {
        Customer customer = customerRepository.getCutomerByPhoneNo(phoneNo);
        if (customer == null) {
            return null;
        }
        return customerMapper.mapCutomerEntityToCustomerDTO(customer);
    }



}
