package com.subi.qborg.subi_qborg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.subi.qborg"})
@EnableJpaRepositories("com.subi.qborg")
@EntityScan("com.subi.qborg")
@EnableAutoConfiguration
public class SubiQborgApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubiQborgApplication.class, args);
		System.out.println("Application Qborg starts  . . . .");
	}

}

