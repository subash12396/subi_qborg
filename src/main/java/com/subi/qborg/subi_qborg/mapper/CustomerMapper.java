package com.subi.qborg.subi_qborg.mapper;

import com.subi.qborg.subi_qborg.dto.CustomerDTO;
import com.subi.qborg.subi_qborg.entities.Customer;
import org.springframework.stereotype.Component;

@Component
public class CustomerMapper {

    public CustomerDTO mapCutomerEntityToCustomerDTO(Customer customer) throws Exception {
        CustomerDTO customerDTO = new CustomerDTO();
        if(customer.getEmailId() != null) {
            customerDTO.setEmailId(customer.getEmailId());
        }
        if(customer.getId()  != null) {
            customerDTO.setId(customer.getId());
        }
        if(customer.getName() != null) {
            customerDTO.setName(customer.getName());
        }
        if(customer.getPhoneNo() != null){
            customerDTO.setPhoneNo(customer.getPhoneNo());
        }
        if(customer.getPassword() !=  null) {
            customerDTO.setPassword(customer.getPassword());
        }
        return  customerDTO;
    }


    public Customer mapCutomerCustomerDTOToEntity(CustomerDTO customerDTO) throws Exception {
        Customer customer = new Customer();
        if(customerDTO.getEmailId() != null) {
            customer.setEmailId(customerDTO.getEmailId());
        }
        if(customerDTO.getId()  != null) {
            customer.setId(customerDTO.getId());
        }
        if(customerDTO.getName() != null) {
            customer.setName(customerDTO.getName());
        }
        if(customerDTO.getPhoneNo() != null){
            customer.setPhoneNo(customerDTO.getPhoneNo());
        }
        if(customerDTO.getPassword() !=  null) {
            customer.setPassword(customerDTO.getPassword());
        }
        return  customer;
    }
}
