package com.subi.qborg.subi_qborg.mapper;

import com.subi.qborg.subi_qborg.dto.VehicleDTO;
import com.subi.qborg.subi_qborg.entities.Vehicle;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VehicleMapper {

    public VehicleDTO mapVehicleEntityToVehicleDTO(Vehicle vehicle) throws  Exception{
        VehicleDTO vehicleDTO = new VehicleDTO();
        if(vehicle.getId() != null) {
            vehicleDTO.setId(vehicle.getId());
        }
        if(vehicle.getDriverName() != null) {
            vehicleDTO.setDriverName(vehicle.getDriverName());
        }
        if(vehicle.getDriverPhoneNo() != null) {
            vehicleDTO.setDriverPhoneNo( vehicle.getDriverPhoneNo());
        }
        if(vehicle.getLocationLat() != null) {
            vehicleDTO.setLocationLat(vehicle.getLocationLat());
        }
        if(vehicle.getLocationLng() != null) {
            vehicleDTO.setLocationLng(vehicle.getLocationLng());
        }
        if(vehicle.getNumber() != null) {
            vehicleDTO.setNumber(vehicle.getNumber());
        }
        if(vehicle.getStatus() !=  null) {
            vehicleDTO.setStatus(vehicle.getStatus());
        }
        if(vehicle.getType() != null) {
            vehicleDTO.setType(vehicle.getType());
        }
        return vehicleDTO;
    }

    public Vehicle mapVehicleDTOToVehicleEntity(VehicleDTO vehicleDTO) throws  Exception{
        Vehicle vehicle = new Vehicle();
        if(vehicleDTO.getId() != null) {
            vehicle.setId(vehicleDTO.getId());
        }
        if(vehicleDTO.getDriverName() != null) {
            vehicle.setDriverName(vehicleDTO.getDriverName());
        }
        if(vehicleDTO.getDriverPhoneNo() != null) {
            vehicle.setDriverPhoneNo( vehicleDTO.getDriverPhoneNo());
        }
        if(vehicleDTO.getLocationLat() != null) {
            vehicle.setLocationLat(vehicleDTO.getLocationLat());
        }
        if(vehicleDTO.getLocationLng() != null) {
            vehicle.setLocationLng(vehicleDTO.getLocationLng());
        }
        if(vehicleDTO.getNumber() != null) {
            vehicle.setNumber(vehicleDTO.getNumber());
        }
        if(vehicleDTO.getStatus() !=  null) {
            vehicle.setStatus(vehicleDTO.getStatus());
        }
        if(vehicleDTO.getType() != null) {
            vehicle.setType(vehicleDTO.getType());
        }
        return vehicle;
    }

    public List<VehicleDTO> mapVehicleEntityListToVehicleDTOList(List<Vehicle> vehicleList) throws Exception {
        List<VehicleDTO> vehicleDTOList = new ArrayList<>();
        for(Vehicle vehicle : vehicleList) {
            VehicleDTO vehicleDTO = mapVehicleEntityToVehicleDTO(vehicle);
            vehicleDTOList.add(vehicleDTO);
        }
        return vehicleDTOList;
    }

}
