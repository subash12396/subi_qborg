package com.subi.qborg.subi_qborg.mapper;

import com.subi.qborg.subi_qborg.dto.CustomerDTO;
import com.subi.qborg.subi_qborg.dto.RidedetailDTO;
import com.subi.qborg.subi_qborg.dto.VehicleDTO;
import com.subi.qborg.subi_qborg.entities.Customer;
import com.subi.qborg.subi_qborg.entities.Ridedetail;
import com.subi.qborg.subi_qborg.entities.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RidedetailMapper {

    @Autowired
    CustomerMapper customerMapper;

    @Autowired
    VehicleMapper vehicleMapper;

     public Ridedetail mapRideDetailDTOToRideDetailEntity(RidedetailDTO ridedetailDTO) throws Exception {
         Ridedetail ridedetail = new Ridedetail();
         if(ridedetailDTO.getFare() != null){
             ridedetail.setFare(ridedetailDTO.getFare());
         }
         if(ridedetailDTO.getSourceLat()  != null) {
             ridedetail.setSourceLat(ridedetailDTO.getSourceLat());
         }
         if(ridedetailDTO.getSourceLng() != null) {
             ridedetail.setSourceLng(ridedetailDTO.getSourceLng());
         }
         if(ridedetailDTO.getDestinationLat() != null) {
             ridedetail.setDestinationLat(ridedetailDTO.getDestinationLat());
         }
         if(ridedetailDTO.getDestinationLng() != null) {
             ridedetail.setDestinationLng(ridedetailDTO.getDestinationLng());
         }
         if(ridedetailDTO.getCustomerDTO() != null) {
             Customer customer = customerMapper.mapCutomerCustomerDTOToEntity(ridedetailDTO.getCustomerDTO());
             ridedetail.setCustomer(customer);
         }
         if(ridedetailDTO.getVehicleDTO() != null) {
             Vehicle vehicle = vehicleMapper.mapVehicleDTOToVehicleEntity(ridedetailDTO.getVehicleDTO());
             ridedetail.setVehicle(vehicle);
         }
         return ridedetail;
     }

    public RidedetailDTO mapRideDetailEntityToRideDetailDTO(Ridedetail ridedetail) throws Exception {
        RidedetailDTO ridedetailDTO = new RidedetailDTO();
        if(ridedetail.getFare() != null){
            ridedetailDTO.setFare(ridedetail.getFare());
        }
        if(ridedetail.getSourceLat()  != null) {
            ridedetailDTO.setSourceLat(ridedetail.getSourceLat());
        }
        if(ridedetail.getSourceLng() != null) {
            ridedetailDTO.setSourceLng(ridedetail.getSourceLng());
        }
        if(ridedetail.getDestinationLat() != null) {
            ridedetailDTO.setDestinationLat(ridedetail.getDestinationLat());
        }
        if(ridedetail.getDestinationLng() != null) {
            ridedetailDTO.setDestinationLng(ridedetail.getDestinationLng());
        }
        if(ridedetail.getCustomer() != null) {
            CustomerDTO customerDTO = customerMapper.mapCutomerEntityToCustomerDTO(ridedetail.getCustomer());
            ridedetailDTO.setCustomerDTO(customerDTO);
        }
        if(ridedetail.getVehicle() != null) {
            VehicleDTO vehicleDTO = vehicleMapper.mapVehicleEntityToVehicleDTO(ridedetail.getVehicle());
            ridedetailDTO.setVehicleDTO(vehicleDTO);
        }
        return ridedetailDTO;
    }




}
