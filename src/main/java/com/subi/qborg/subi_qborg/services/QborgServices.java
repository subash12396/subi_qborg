package com.subi.qborg.subi_qborg.services;

import com.subi.qborg.subi_qborg.dto.QborgResponse;
import com.subi.qborg.subi_qborg.process.DistanceAPIProcess;
import com.subi.qborg.subi_qborg.process.QborgProcess;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api")
public class QborgServices {


    @Autowired
    private QborgProcess qborgProcess;

    @Autowired
    private DistanceAPIProcess distanceAPIProcess;

    //Just used for internal code.  No API call.  Called inside the catch block in all apis. To ride the error status.
    public QborgResponse throwErrorOnCatch(Exception ex,String response){
        System.out.println(ex);
        QborgResponse qborgResponse = new QborgResponse("Error",response);
        return qborgResponse;
    }

    public QborgResponse successResponse(Object response){
        QborgResponse qborgResponse = new QborgResponse("Success",response);
        return qborgResponse;
    }

    //common login for both customer and driver.
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public QborgResponse login(@RequestBody JSONObject jsonObject) {
        JSONObject response = new JSONObject();
        try{
            response = qborgProcess.login(jsonObject);
        }catch (Exception ex) {
            return throwErrorOnCatch(ex,"Login Failed");
        }
        return successResponse(response);
    }

    //API need to write with new logic and set up.
    @RequestMapping(value = "/forgotpassword", method = RequestMethod.POST)
    @ResponseBody
    public QborgResponse forgotPassword(@RequestBody JSONObject  jsonObject) {
        JSONObject response = new JSONObject();
        try {
            response = qborgProcess.forgotPassword(jsonObject);
        }catch (Exception ex) {
            return throwErrorOnCatch(ex,"Reset password failed" );
        }
        return successResponse(response);
    }

    //Customer registration.
    @RequestMapping(value="registercustomer",method= RequestMethod.POST)
    @ResponseBody
    public QborgResponse customerRegistration(@RequestBody JSONObject jsonObject) {
        String response = new String();
        try {
            response = qborgProcess.customerRegistration(jsonObject);
        }catch (Exception ex) {
            response ="Customer Registration failed";
            return throwErrorOnCatch(ex,response);
        }
        return successResponse(response);
    }

    // Vehicle resgistration.
    @RequestMapping(value = "/registervehicle", method = RequestMethod.POST)
    @ResponseBody
    public QborgResponse vehicleRegistration(@RequestBody  JSONObject jsonObject) {
        String response = new String();
        try {
            response = qborgProcess.vehicleRegistration(jsonObject);
        }catch (Exception ex) {
            response ="Vehicle Registration failed";
            return throwErrorOnCatch(ex,response);
        }
        return successResponse(response);
    }

    //Update ride details
    @RequestMapping(value="/ridedetails",method = RequestMethod.POST)
    @ResponseBody
    public QborgResponse updateRideDetails(@RequestBody JSONObject jsonObject) {
        String response = new String();
        try {
            response = qborgProcess.updateRideDetails(jsonObject);
        }catch (Exception ex) {
            response ="false";
            return throwErrorOnCatch(ex,response);
        }
        return successResponse(response);
    }

    //Send booking details to customer
    @RequestMapping(value = "/bookingdetailsforcustomer",method = RequestMethod.POST)
    @ResponseBody
    public QborgResponse getBookingDetailsForCustomer(@RequestBody JSONObject jsonObject) {
        JSONObject response = new JSONObject();
        try {
            response = qborgProcess.getBookingDetailsForCustomer(jsonObject);
        }catch (Exception ex) {
            return throwErrorOnCatch(ex,"Error in Fetching Details");
        }
        return successResponse(response);
    }

    //Send customer details to driver
    @RequestMapping(value="/bookingdetailsfordriver", method = RequestMethod.POST)
    @ResponseBody
    public QborgResponse getBookingDetailsForDriver(@RequestBody JSONObject jsonObject){
        JSONObject response = new JSONObject();
        try{
            response = qborgProcess.getBookingDetailsForDriver(jsonObject);
        }catch (Exception ex) {
            return throwErrorOnCatch(ex,"Error in Fetching Details");
        }
        return successResponse(response);
    }

    //Calculate the distance, fare and cab get booked to nearest and active driver.
    @RequestMapping(value="/bookvehicle",method= RequestMethod.POST)
    @ResponseBody
    public QborgResponse bookVehicle(@RequestBody JSONObject jsonObject) {
        String response = new String();
        try{
            response = qborgProcess.bookVehicle(jsonObject);
        }catch (Exception ex) {
            return throwErrorOnCatch(ex,"Booking Failed. Please try again");
        }
        return successResponse(response);
    }


}