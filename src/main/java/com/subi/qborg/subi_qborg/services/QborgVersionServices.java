package com.subi.qborg.subi_qborg.services;


import com.subi.qborg.subi_qborg.dao.CustomerDao;
import com.subi.qborg.subi_qborg.process.*;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class QborgVersionServices {

    @Autowired
    private VersionProcess versionProcess;

    @Autowired
    private CustomerDao customerDao;

    @RequestMapping(value = "/version", method = RequestMethod.GET, produces = "application/json")
    public JSONObject getVersion() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject= versionProcess.getVersion();
        }catch(Exception exception ) {
            jsonObject.put("Error",exception);
        }
        return jsonObject;
    }
}
