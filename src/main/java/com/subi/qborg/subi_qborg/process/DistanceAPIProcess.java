
package com.subi.qborg.subi_qborg.process;

import com.squareup.okhttp.*;
import java.io.IOException;


import org.json.simple.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Transactional
@Component
public class DistanceAPIProcess {

    //Create object for the OkHttp class and call http request by make use of the object
    OkHttpClient client = new OkHttpClient();

    //Make http calls.
    public String makeHTTPRequest(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    //Get the Distance Object.
    public Double getDistance(Double sourceLat,Double sourceLng, Double destinationLat, Double destinationLng) throws Exception{
        String url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins="+sourceLat+ "," +sourceLng+"&destinations="+destinationLat+","+destinationLng+"&key=AIzaSyAs1_8Pb8jddmEi917hXaZMj9BfUuGqc44";
        //String googleAPIResponse = makeHTTPRequest(url);
        String googleAPIResponse = getGoogleResponse();

        //get the Distance from the whole google response
         JSONObject distanceObject = new JSONObject(googleAPIResponse).getJSONArray("rows")
                .getJSONObject(0)
                .getJSONArray("elements")
                .getJSONObject(0)
                .getJSONObject("distance");

        //Distance of each driver and the source location.
        String distanceValue = distanceObject.get("value").toString();
        return Double.parseDouble(distanceValue);
    }

    //Get the Souce and Destination Object
    public Map getSourceAndDestinationText(Double sourceLat, Double sourceLng, Double destinationLat, Double destinationLng) throws Exception {
        String url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + sourceLat + "," + sourceLng + "&destinations=" + destinationLat + "," + destinationLng + "&key=AIzaSyAs1_8Pb8jddmEi917hXaZMj9BfUuGqc44";
        //String googleAPIResponse = makeHTTPRequest(url);

        String googleAPIResponse = getGoogleResponse();

        Map<String,String> locations = new HashMap<>();
        //Get the Source & Destination location.

        String source = new JSONObject(googleAPIResponse).getJSONArray("destination_addresses").get(0).toString();
        String destination = new JSONObject(googleAPIResponse).getJSONArray("origin_addresses").get(0).toString();

        //set location into global variable.
        locations.put("source",source);
        locations.put("destination",destination);
        return locations;
    }

    //get Google Response (Test value)
    public String getGoogleResponse(){
        String googleAPIResponse = "{\n" +
                "   \"destination_addresses\" : [\n" +
                "      \"21/2, Bannerghatta Main Rd, SR Krishnappa Garden, Hombegowda Nagar, Bengaluru, Karnataka 560029, India\"\n" +
                "   ],\n" +
                "   \"origin_addresses\" : [\n" +
                "      \"116a, Elephant Rock Rd, Jayanagar East, Jaya Nagar 1st Block, Jayanagar, Bengaluru, Karnataka 560011, India\"\n" +
                "   ],\n" +
                "   \"rows\" : [\n" +
                "      {\n" +
                "         \"elements\" : [\n" +
                "            {\n" +
                "               \"distance\" : {\n" +
                "                  \"text\" : \"4.2 km\",\n" +
                "                  \"value\" : 2900\n" +
                "               },\n" +
                "               \"duration\" : {\n" +
                "                  \"text\" : \"14 mins\",\n" +
                "                  \"value\" : 819\n" +
                "               },\n" +
                "               \"status\" : \"OK\"\n" +
                "            }\n" +
                "         ]\n" +
                "      }\n" +
                "   ],\n" +
                "   \"status\" : \"OK\"\n" +
                "}";
        return googleAPIResponse;
    }
}

