package com.subi.qborg.subi_qborg.process;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class VersionProcess {
    public JSONObject getVersion() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Version","0.1");
        return jsonObject;
    }
}
