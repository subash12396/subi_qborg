package com.subi.qborg.subi_qborg.process;


import com.subi.qborg.subi_qborg.dao.CustomerDao;
import com.subi.qborg.subi_qborg.dao.RidedetailDao;
import com.subi.qborg.subi_qborg.dao.VehicleDao;
import com.subi.qborg.subi_qborg.dto.CustomerDTO;
import com.subi.qborg.subi_qborg.dto.RidedetailDTO;
import com.subi.qborg.subi_qborg.dto.VehicleDTO;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Component
public class QborgProcess {

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private VehicleDao vehicleDao;

    @Autowired
    RidedetailDao ridedetailsDao;

    @Autowired
    DistanceAPIProcess distanceAPIProcess;

    private Double MINIMUM_DISTANCE = Double.MAX_VALUE;

    private VehicleDTO nearestDriver = null;

    private String sourceLocation =  new String();

    private String destinationLocaton =  new String();

    private Double fare = 0.0;

    //getter & setter
    public VehicleDTO getNearestDriver() {
        return nearestDriver;
    }

    public void setNearestDriver(VehicleDTO nearestDriver) {
        this.nearestDriver = nearestDriver;
    }

    public Double getFare() {
        return fare;
    }

    public void setFare(Double fare) {
        this.fare = fare;
    }

    public JSONObject login(JSONObject jsonObject) throws  Exception {
        JSONObject response = new JSONObject();
        Boolean isInvalid = false;

        String userId = (String) jsonObject.get("userId");
        String passcode = (String) jsonObject.get("password");
        CustomerDTO customerDTO = isUserExist(userId);
        if (customerDTO != null) {
            if (customerDTO.getPassword().equals(passcode)) {
                response.put("userIs", "customer");
                response.put("userId", userId);
            } else {
                isInvalid = true;
            }
        } else {
            VehicleDTO vehicleDTO = vehicleDao.getVehicleDTOByPhoneNo(userId);
            if(vehicleDTO != null) {
                if("fancy".equals(passcode)){
                    response.put("userIs", "driver");
                    response.put("userId", userId);
                }
                else{
                    isInvalid = true;
                }
            }
            else {
                isInvalid = true;
            }
        }
        if(isInvalid) {
            response.put("userIs","Invalid");
            response.put("userId",-1);
        }
        return response;
    }

    //Need to write new logic and setups.
    public JSONObject forgotPassword(JSONObject jsonObject) throws Exception {
        JSONObject respnonse = new JSONObject();
        return respnonse;
    }

    public String customerRegistration(JSONObject jsonObject) throws Exception{
        String response = new String();
        CustomerDTO customerDTO = isUserExist((String)jsonObject.get("emailId"));
        if (customerDTO != null) {
            return "Already registered.";
        }
        else if(isPhoneNoRegistered((String)jsonObject.get("phoneNo"),"CUSTOMER")){
            return "Phone Number Already registered.";
        }
        else {
            customerDTO = new CustomerDTO();
        }
        customerDTO.setPassword((String) jsonObject.get("password"));
        customerDTO.setPhoneNo((String) jsonObject.get("phoneNo"));
        customerDTO.setName((String)jsonObject.get("name"));
        customerDTO.setEmailId((String)jsonObject.get("emailId"));
        if(customerDao.saveNewCustomer(customerDTO)) {
            response = "Customer Registration done successfully";
        }
        return response;
    }

    public String vehicleRegistration(JSONObject jsonObject) throws Exception{
        String response = new String();
        VehicleDTO vehicleDTO = isVehicleExist((String)jsonObject.get("vehicleNumber"));
        if (vehicleDTO != null) {
            return "Vehicle already registered.";
        }
        else if(isPhoneNoRegistered((String)jsonObject.get("driverPhoneNo"),"DRIVER")){
            return "Driver phone number already registered.";
        }
        else {
            vehicleDTO = new VehicleDTO();
        }
        vehicleDTO.setNumber((String) jsonObject.get("vehicleNumber"));
        vehicleDTO.setLocationLng((Double) jsonObject.get("locationLng"));
        vehicleDTO.setLocationLat((Double) jsonObject.get("locationLat"));
        vehicleDTO.setDriverPhoneNo((String)jsonObject.get("driverPhoneNo"));
        vehicleDTO.setType((String)jsonObject.get("vehicleType"));
        vehicleDTO.setDriverName((String) jsonObject.get("driverName"));
        vehicleDTO.setStatus((Integer) jsonObject.get("status"));

        if(vehicleDao.saveNewVehicle(vehicleDTO)) {
            response = "Vehicle Registration done successfully";
        }
        return response;
    }


    public CustomerDTO isUserExist(String userId) throws Exception {
        CustomerDTO customerDTO = customerDao.getCustomerDTOByEmailId(userId);
        return  customerDTO;
    }

    public boolean isPhoneNoRegistered(String phoneNo,String userIs) throws  Exception{
        if (userIs.equals("CUSTOMER")) {
            CustomerDTO customerDTO = customerDao.getCustomerDTOByPhoneNo(phoneNo);
            if (customerDTO == null) {
                return false;
            }
        } else{
            VehicleDTO vehicleDTO = vehicleDao.getVehicleDTOByPhoneNo(phoneNo);
            if(vehicleDTO == null) {
                return false;
            }
        }
        return  true;
    }

    public VehicleDTO isVehicleExist(String vehicleNumber) throws Exception {
        VehicleDTO vehicleDTO  = vehicleDao.getVehicleDTOByVehicleNumber(vehicleNumber);
        return  vehicleDTO;
    }

    //Save into database. After a vehicle booked
    public String updateRideDetails(JSONObject jsonObject) throws  Exception{
        String response = new String();
        RidedetailDTO ridedetailDTO = new RidedetailDTO();
        CustomerDTO customerDTO = isUserExist((String)jsonObject.get("customerId"));
        VehicleDTO vehicleDTO =  (VehicleDTO) jsonObject.get("vehicleId");
        ridedetailDTO.setSourceLat((Double)jsonObject.get("customerSourceLat"));
        ridedetailDTO.setDestinationLat((Double)jsonObject.get("customerDestinationLat"));
        ridedetailDTO.setSourceLng((Double)jsonObject.get("customerSourceLng"));
        ridedetailDTO.setDestinationLng((Double)jsonObject.get("customerDestinationLng"));
        ridedetailDTO.setFare((Double)jsonObject.get("fare"));
        ridedetailDTO.setCustomerDTO(customerDTO);
        ridedetailDTO.setVehicleDTO(vehicleDTO);
        if(ridedetailsDao.saveNewRideDetail(ridedetailDTO)){
            response="true";
        }
        return response;
    }

    //get booking details for customer screen.
    public JSONObject getBookingDetailsForCustomer(JSONObject jsonObject) throws  Exception{
        JSONObject response = new JSONObject();
        CustomerDTO customerDTO =  customerDao.getCustomerDTOByEmailId((String)jsonObject.get("emailId"));
        if(customerDTO != null) {
            RidedetailDTO ridedetailDTO = ridedetailsDao.getRideDetailByCustomer(customerDTO);
            response.put("driverName",ridedetailDTO.getVehicleDTO().getDriverName());
            response.put("driverPhoneNo",ridedetailDTO.getVehicleDTO().getDriverPhoneNo());
            response.put("vehicle",ridedetailDTO.getVehicleDTO().getType());
            response.put("vehicleNumber",ridedetailDTO.getVehicleDTO().getNumber());
            response.put("fare",ridedetailDTO.getFare());
            // Set the Global variables to give the response to UI (Customer details and Vehicle details Call)
            Map<String,String> locations = new HashMap<>();
            locations = distanceAPIProcess.getSourceAndDestinationText( ridedetailDTO.getSourceLat(),ridedetailDTO.getSourceLng(),ridedetailDTO.getDestinationLat(),ridedetailDTO.getDestinationLng());
            response.put("source",locations.get("source"));
            response.put("destination",locations.get("destination"));
        }
        return response;
    }

    //Get the Booking details.
    public JSONObject getBookingDetailsForDriver(JSONObject jsonObject) throws  Exception{
        JSONObject response = new JSONObject();
        String vehicleNumber = (String) jsonObject.get("vehicleNumber");
        VehicleDTO vehicleDTO = vehicleDao.getVehicleDTOByVehicleNumber(vehicleNumber);
        if(vehicleDTO.getStatus() == 1) {
            RidedetailDTO ridedetailDTO =  ridedetailsDao.getRideDetailByVehicle(vehicleDTO);
            CustomerDTO customerDTO = ridedetailDTO.getCustomerDTO();
            response.put("customerName",customerDTO.getName());
            response.put("customerPhoneNo",customerDTO.getPhoneNo());
            response.put("fare",ridedetailDTO.getFare());
            // Set the Global variables to give the response to UI (Customer details and Vehicle details Call)
            Map<String,String> locations = new HashMap<>();
            locations = distanceAPIProcess.getSourceAndDestinationText( ridedetailDTO.getSourceLat(),ridedetailDTO.getSourceLng(),ridedetailDTO.getDestinationLat(),ridedetailDTO.getDestinationLng());
            response.put("source",locations.get("source"));
            response.put("destination",locations.get("destination"));
        }
        return response;
    }

    //bookVehicle
    public String bookVehicle(JSONObject jsonObject) throws  Exception{
        setNearestDriver(null);
        Double sourceLat = (Double) jsonObject.get("sourceLat");
        Double sourceLng = (Double) jsonObject.get("sourceLng");
        Double destinationLat = (Double) jsonObject.get("destinationLat");
        Double destinationLng = (Double) jsonObject.get("destinationLng");
        String customerId = (String) jsonObject.get("customerId");
        List<VehicleDTO> vehicleDTOList = getAllAvailableVehicles();
        if(vehicleDTOList.size() == 0) {
            return "No rides are available";
        }
        for(VehicleDTO vehicleDTO : vehicleDTOList) {
            Double distance = distanceAPIProcess.getDistance(sourceLat,sourceLng,vehicleDTO.getLocationLat(),vehicleDTO.getLocationLng());
            if(distance <= 3000) {
                Boolean checkNearestDriver = findNearestDriver(distance);
                if(checkNearestDriver){
                    setNearestDriver(vehicleDTO); //Set the nearest driver.
                }
            }
        }
        if(getNearestDriver() == null) {
            return "No rides are available";
        } else{
            //Calculate the fare.
            Double distance = distanceAPIProcess.getDistance(sourceLat,sourceLng,destinationLat,destinationLng);
            setFare(distance*1.4); //set fare
        }
        JSONObject updateRideObject = new JSONObject();
        updateRideObject.put("customerSourceLat",sourceLat);
        updateRideObject.put("customerSourceLng",sourceLng);
        updateRideObject.put("customerDestinationLat",destinationLat);
        updateRideObject.put("customerDestinationLng",destinationLng);
        updateRideObject.put("fare",getFare());
        updateRideObject.put("customerId",customerId);
        updateRideObject.put("vehicleId",getNearestDriver());
        updateRideDetails(updateRideObject);

        VehicleDTO vehicleDTO = getNearestDriver();
        Integer status = 1;
        vehicleDao.updateVehicleStatus(vehicleDTO,status);
        return "Vehicle Booked.";
    }

    //set distance.
    public Boolean findNearestDriver(Double distance){
        if(MINIMUM_DISTANCE > distance) {
            MINIMUM_DISTANCE = distance;
            return true;
        }
        return false;
    }

    //get all the driverDTOs, who status is 0.
    public List<VehicleDTO> getAllAvailableVehicles()throws Exception {
        return  vehicleDao.getAllAvailableVehicles();
    }
}

