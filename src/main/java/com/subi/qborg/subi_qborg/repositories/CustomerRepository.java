package com.subi.qborg.subi_qborg.repositories;


import com.subi.qborg.subi_qborg.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface CustomerRepository  extends JpaRepository<Customer, Integer> {

    @Query("SELECT c from Customer c")
    public List<Customer> findAll();

    @Query("SELECT c from Customer c where c.id = :id")
    public Customer findID(@Param("id") int id);

    @Query("SELECT c.name from Customer c where c.id = :id")
    public String findName(@Param("id") int id);

    @Query(value = "Select c from Customer c where c.emailId = ?1")
    public Customer getCutomerByEmailId(String emailId);

    @Query(value = "Select c from Customer c where c.phoneNo = ?1")
    public Customer getCutomerByPhoneNo(String phoneNo);



}
