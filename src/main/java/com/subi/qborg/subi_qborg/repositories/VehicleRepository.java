package com.subi.qborg.subi_qborg.repositories;


import com.subi.qborg.subi_qborg.entities.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface VehicleRepository extends JpaRepository<Vehicle, Integer> {

    @Query("SELECT v from Vehicle v")
    public List<Vehicle> findAll();

    @Query("SELECT c from Vehicle c where c.id = :id")
    public Vehicle findID(@Param("id") int id);

    @Query("select v from Vehicle v where v.status = 0")
    public List<Vehicle> gettAllAvailableVehicle();

    @Modifying
    @Query("UPDATE Vehicle c set c.status= ?2 where c.id = ?1")
    public void updateVehicleStatus(Integer vehicleId,Integer status);

    @Query("SELECT c from Vehicle c where c.driverPhoneNo = ?1")
    public Vehicle getVehicleByPhoneNo(String PhoneNo);

    @Query("SELECT c from Vehicle c where c.number = ?1")
    public Vehicle getVehicleDTOByVehicleNumber(String vehicleNumber);

    @Query("SELECT c.status from Vehicle c where c.id = :id")
    public int status(@Param("id") int id);

    @Query("SELECT c.driverName from Vehicle c where c.id = :id")
    public String findname(@Param("id") int id);
}
