package com.subi.qborg.subi_qborg.repositories;


import com.subi.qborg.subi_qborg.entities.Customer;
import com.subi.qborg.subi_qborg.entities.Ridedetail;
import com.subi.qborg.subi_qborg.entities.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface RidedetailRepository extends JpaRepository<Ridedetail, Integer> {

 /*   @Query("select r from Ridedetail r")
    public List<Ridedetail> findAll();

    @Query(value = "select rd.CustomerID from ridedetails rd where rd.VehicleID = ?1",nativeQuery = true)
    public int driver(int id);

    @Query(value = "select * from ridedetails rd where rd.CustomerID = ?1",nativeQuery = true)
    public List<Ridedetail> findCustomer(int customerid);*/


   @Query(value = "Select * from ridedetail rd where rd.vehicle_id = ?1 ORDER BY rd.id DESC LIMIT 1",nativeQuery = true)
   public Ridedetail getRideDetailByVehicle(Integer vehicleId);

   @Query(value = "Select * from ridedetail rd where rd.customer_id = ?1 ORDER BY rd.id DESC LIMIT 1",nativeQuery = true)
   public Ridedetail getRideDetailByCustomer(Integer customerId);






}
