package com.subi.qborg.subi_qborg.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the vehicle database table.
 * 
 */
@Entity
@Table(name="vehicle")
@NamedQuery(name="Vehicle.findAll", query="SELECT v FROM Vehicle v")
public class Vehicle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "driver_name")
	private String driverName;

	@Column(name = "driver_phone_no")
	private String driverPhoneNo;

	@Column(name = "location_lat")
	private Double locationLat;

	@Column(name = "location_lng")
	private Double locationLng;

	@Column(name = "number")
	private String number;

	@Column(name = "status")
	private Integer status;

	@Column(name = "type")
	private String type;

	//bi-directional many-to-one association to Ridedetail
	@OneToMany(mappedBy="vehicle")
	private List<Ridedetail> ridedetails;

	public Vehicle() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDriverName() {
		return this.driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getDriverPhoneNo() {
		return this.driverPhoneNo;
	}

	public void setDriverPhoneNo(String driverPhoneNo) {
		this.driverPhoneNo = driverPhoneNo;
	}

	public Double getLocationLat() {
		return this.locationLat;
	}

	public void setLocationLat(Double locationLat) {
		this.locationLat = locationLat;
	}

	public Double getLocationLng() {
		return this.locationLng;
	}

	public void setLocationLng(Double locationLng) {
		this.locationLng = locationLng;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Ridedetail> getRidedetails() {
		return this.ridedetails;
	}

	public void setRidedetails(List<Ridedetail> ridedetails) {
		this.ridedetails = ridedetails;
	}

	public Ridedetail addRidedetail(Ridedetail ridedetail) {
		getRidedetails().add(ridedetail);
		ridedetail.setVehicle(this);

		return ridedetail;
	}

	public Ridedetail removeRidedetail(Ridedetail ridedetail) {
		getRidedetails().remove(ridedetail);
		ridedetail.setVehicle(null);

		return ridedetail;
	}

}