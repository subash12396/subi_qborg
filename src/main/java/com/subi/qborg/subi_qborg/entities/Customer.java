package com.subi.qborg.subi_qborg.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the customer database table.
 * 
 */
@Entity
@Table(name="customer")
@NamedQuery(name="Customer.findAll", query="SELECT c FROM Customer c")
public class Customer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "email_id")
	private String emailId;

	@Column(name = "name")
	private String name;

	@Column(name = "password")
	private String password;

	@Column(name = "phone_no")
	private String phoneNo;

	//bi-directional many-to-one association to Ridedetail
	@OneToMany(mappedBy="customer")
	private List<Ridedetail> ridedetails;

	public Customer() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmailId() {
		return this.emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNo() {
		return this.phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public List<Ridedetail> getRidedetails() {
		return this.ridedetails;
	}

	public void setRidedetails(List<Ridedetail> ridedetails) {
		this.ridedetails = ridedetails;
	}

	public Ridedetail addRidedetail(Ridedetail ridedetail) {
		getRidedetails().add(ridedetail);
		ridedetail.setCustomer(this);

		return ridedetail;
	}

	public Ridedetail removeRidedetail(Ridedetail ridedetail) {
		getRidedetails().remove(ridedetail);
		ridedetail.setCustomer(null);

		return ridedetail;
	}

}