package com.subi.qborg.subi_qborg.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ridedetail database table.
 * 
 */
@Entity
@Table(name="ridedetail")
@NamedQuery(name="Ridedetail.findAll", query="SELECT r FROM Ridedetail r")
public class Ridedetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "destination_lat")
	private Double destinationLat;

	@Column(name = "destination_lng")
	private Double destinationLng;

	@Column(name = "fare")
	private Double fare;

	@Column(name = "source_lat")
	private Double sourceLat;

	@Column(name = "source_lng")
	private Double sourceLng;

	//bi-directional many-to-one association to Customer
	@ManyToOne
	@JoinColumn(name="customer_id")
	private Customer customer;

	//bi-directional many-to-one association to Vehicle
	@ManyToOne
	@JoinColumn(name="vehicle_id")
	private Vehicle vehicle;

	public Ridedetail() {
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getDestinationLat() {
		return destinationLat;
	}

	public void setDestinationLat(Double destinationLat) {
		this.destinationLat = destinationLat;
	}

	public Double getDestinationLng() {
		return destinationLng;
	}

	public void setDestinationLng(Double destinationLng) {
		this.destinationLng = destinationLng;
	}

	public Double getFare() {
		return fare;
	}

	public void setFare(Double fare) {
		this.fare = fare;
	}

	public Double getSourceLat() {
		return sourceLat;
	}

	public void setSourceLat(Double sourceLat) {
		this.sourceLat = sourceLat;
	}

	public Double getSourceLng() {
		return sourceLng;
	}

	public void setSourceLng(Double sourceLng) {
		this.sourceLng = sourceLng;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Vehicle getVehicle() {
		return this.vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

}