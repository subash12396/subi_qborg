package com.subi.qborg.subi_qborg.dto;

public class QborgResponse {

    private String status;

    private Object data;
    public QborgResponse (String status,Object response) {
       this.status = status;
       this.data = response;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}

